# Android Test Gorilla Logic


### Dario Fernando Chamorro Vela
dario.chamorro.v@gmail.com

57 3017790935

&nbsp;

## Requirements to run the application
- Android Studio 4.1.2

&nbsp;


## Solution Architecture

In order to propose a solution that is tolerant to change and that is easy to scale over time, I was decided to use Clean Architecture, segmenting the application into three layers Presentation, Domain and Data.


### **Domain**
The domain layer implements the Business logic and rules

- **Models**:
 The Models can implement structures and business rules, in this case the information of the Post, Comments and Detail

- **Use Case**:
The use cases allow interaction with the architecture and implement the business logic

- **Repository**:
The repositories abstract the sources of information, in the domain layer only the behavior is defined but its implementation is unknown, this so that the data layer depends on the domain layer

### **Presentation**
The presentation layer includes the Activities or Fragments views, and state management through ViewModel and LiveData

### **Data**
The data layer implements the repositories defined in the domain layer and it define the behavior of the data sources

&nbsp;


## Code Quality

### **Ktlint**
In order to have a code with the same style and format, [ktlint](https://github.com/pinterest/ktlint) was added that allows adding rules in the code, this is executed before to build task

### **Continuous Integration**
Added gitlab-ci.yml to execute scripts for build and test in each Merge Request

&nbsp;

## Libraries
Official android libraries were used or that are widely used by the development community.

### **Retrofit**
Library to implement an Http Client through interfaces which is compatible with the Dependency Inversion pattern

### **Room**
Library for data access with SQLite, official for Android, allows creating DAOs with interfaces so it is compatible with the Dependency Inversion pattern

### **Android ViewModel** 
Set of official android libraries for the use of ViewModels to handle the view state

### **Android LiveData** 
Set of official android libraries emit updates of the state from ViewModel to View

### **Hilt**
Official android library for dependency injection with Dagger, it greatly facilitates the injection of dependencies so it is very useful

&nbsp;


## Good Practices

In the development of the project, it was sought to implement a set of good practices:

 - CLean Architecture
 - SOLID Patterns
 - KtLint
 - Continuous Integration
