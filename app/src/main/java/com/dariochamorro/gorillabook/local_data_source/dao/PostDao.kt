package com.dariochamorro.gorillabook.local_data_source.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.dariochamorro.gorillabook.local_data_source.entities.PostEntity
import kotlinx.coroutines.flow.Flow

@Dao
interface PostDao {

    @Insert
    suspend fun insert(post: PostEntity)

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insertMany(posts: List<PostEntity>)

    @Query("SELECT * FROM posts ORDER BY unixTimestamp DESC")
    fun listAllDescending(): Flow<List<PostEntity>>
}
