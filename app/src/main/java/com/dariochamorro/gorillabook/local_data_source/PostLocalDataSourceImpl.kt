package com.dariochamorro.gorillabook.local_data_source

import com.dariochamorro.gorillabook.data.datasources.PostLocalDataSource
import com.dariochamorro.gorillabook.domain.models.Post
import com.dariochamorro.gorillabook.local_data_source.dao.PostDao
import com.dariochamorro.gorillabook.local_data_source.mappers.PostEntityMapper
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import javax.inject.Inject

class PostLocalDataSourceImpl @Inject constructor(
    private val dao: PostDao,
    private val mapper: PostEntityMapper
) : PostLocalDataSource {
    override suspend fun insertPost(post: Post) {
        val entity = mapper.fromModel(post)
        dao.insert(entity)
    }

    override suspend fun insertManyPosts(post: List<Post>) {
        val entities = mapper.fromListModel(post)
        dao.insertMany(entities)
    }

    override fun listPosts(): Flow<List<Post>> = dao.listAllDescending()
        .map(mapper::toListModel)
}
