package com.dariochamorro.gorillabook.local_data_source.database

import androidx.room.Database
import androidx.room.RoomDatabase
import com.dariochamorro.gorillabook.local_data_source.dao.PostDao
import com.dariochamorro.gorillabook.local_data_source.entities.PostEntity

@Database(version = 1, entities = [PostEntity::class])
abstract class AppDatabase : RoomDatabase() {

    abstract fun postDao(): PostDao
}
