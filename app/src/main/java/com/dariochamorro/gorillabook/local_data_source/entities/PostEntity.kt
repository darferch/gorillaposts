package com.dariochamorro.gorillabook.local_data_source.entities

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "posts")
data class PostEntity(
    @PrimaryKey(autoGenerate = true)
    val id: Long? = null,
    val firstName: String = "",
    val lastName: String = "",
    val postBody: String = "",
    val unixTimestamp: Long = 0,
    val image: String? = null
)
