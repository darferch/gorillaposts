package com.dariochamorro.gorillabook.local_data_source.mappers

import com.dariochamorro.gorillabook.domain.common.ModelMapper
import com.dariochamorro.gorillabook.domain.models.Post
import com.dariochamorro.gorillabook.local_data_source.entities.PostEntity
import java.util.Date
import javax.inject.Inject

class PostEntityMapper @Inject constructor() : ModelMapper<Post, PostEntity>() {
    override fun toModel(data: PostEntity): Post = Post(
        id = data.id,
        firstName = data.firstName,
        lastName = data.lastName,
        postBody = data.postBody,
        date = Date(data.unixTimestamp),
        image = data.image
    )

    override fun fromModel(model: Post): PostEntity = PostEntity(
        id = model.id,
        firstName = model.firstName,
        lastName = model.lastName,
        postBody = model.postBody,
        unixTimestamp = model.date.time,
        image = model.image
    )
}
