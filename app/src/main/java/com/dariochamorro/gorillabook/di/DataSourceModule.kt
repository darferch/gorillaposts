package com.dariochamorro.gorillabook.di

import com.dariochamorro.gorillabook.data.datasources.PostLocalDataSource
import com.dariochamorro.gorillabook.data.datasources.PostRemoteDataSource
import com.dariochamorro.gorillabook.local_data_source.PostLocalDataSourceImpl
import com.dariochamorro.gorillabook.remote_data_source.PostRemoteDataSourceImpl
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
abstract class DataSourceModule {

    @Binds
    @Singleton
    abstract fun bindPostRemoteDataSource(postRemoteDataSourceImpl: PostRemoteDataSourceImpl): PostRemoteDataSource

    @Binds
    @Singleton
    abstract fun bindPostLocalDataSource(postLocalDataSourceImpl: PostLocalDataSourceImpl): PostLocalDataSource
}
