package com.dariochamorro.gorillabook.di

import com.dariochamorro.gorillabook.local_data_source.dao.PostDao
import com.dariochamorro.gorillabook.local_data_source.database.DatabaseProvider
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object DatabaseModule {

    @Provides
    @Singleton
    fun providePostDao(databaseProvider: DatabaseProvider): PostDao =
        databaseProvider.database.postDao()
}
