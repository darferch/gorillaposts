package com.dariochamorro.gorillabook.di

import com.dariochamorro.gorillabook.remote_data_source.apis.ApiProvider
import com.dariochamorro.gorillabook.remote_data_source.apis.PostApi
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
object NetworkModule {

    @Provides
    fun providePostApi(apiProvider: ApiProvider): PostApi = apiProvider.create(PostApi::class.java)
}
