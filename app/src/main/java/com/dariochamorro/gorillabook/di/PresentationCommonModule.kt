package com.dariochamorro.gorillabook.di

import com.dariochamorro.gorillabook.presentation.common.errors.ErrorMessage
import com.dariochamorro.gorillabook.presentation.common.errors.ErrorMessageImpl
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
abstract class PresentationCommonModule {

    @Binds
    @Singleton
    abstract fun bindErrorMessage(errorMessageImpl: ErrorMessageImpl): ErrorMessage
}
