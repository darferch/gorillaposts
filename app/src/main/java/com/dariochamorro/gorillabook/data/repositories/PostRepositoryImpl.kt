package com.dariochamorro.gorillabook.data.repositories

import com.dariochamorro.gorillabook.data.datasources.PostLocalDataSource
import com.dariochamorro.gorillabook.data.datasources.PostRemoteDataSource
import com.dariochamorro.gorillabook.domain.common.AsyncResult
import com.dariochamorro.gorillabook.domain.common.Errors
import com.dariochamorro.gorillabook.domain.models.Post
import com.dariochamorro.gorillabook.domain.repositories.PostRepository
import kotlinx.coroutines.flow.Flow
import java.io.IOException
import javax.inject.Inject

class PostRepositoryImpl @Inject constructor(
    private val localDataSource: PostLocalDataSource,
    private val remoteDataSource: PostRemoteDataSource
) : PostRepository {

    override fun listPosts(): Flow<List<Post>> = localDataSource.listPosts()

    override suspend fun loadPost(): AsyncResult<Unit> {
        return try {
            val posts = remoteDataSource.getPosts()
            localDataSource.insertManyPosts(posts)
            AsyncResult.success(Unit)
        } catch (e: IOException) {
            AsyncResult.error(Errors.NetworkError)
        } catch (e: Exception) {
            AsyncResult.error(Errors.UnknownError)
        }
    }

    override suspend fun insertPost(post: Post): AsyncResult<Unit> {
        return try {
            localDataSource.insertPost(post)
            AsyncResult.success(Unit)
        } catch (e: Exception) {
            AsyncResult.error(Errors.DatabaseError)
        }
    }
}
