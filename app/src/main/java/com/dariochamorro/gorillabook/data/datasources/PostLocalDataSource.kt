package com.dariochamorro.gorillabook.data.datasources

import com.dariochamorro.gorillabook.domain.models.Post
import kotlinx.coroutines.flow.Flow

interface PostLocalDataSource {

    suspend fun insertPost(post: Post)

    suspend fun insertManyPosts(post: List<Post>)

    fun listPosts(): Flow<List<Post>>
}
