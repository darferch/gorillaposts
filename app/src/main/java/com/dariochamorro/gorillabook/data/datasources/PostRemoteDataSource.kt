package com.dariochamorro.gorillabook.data.datasources

import com.dariochamorro.gorillabook.domain.models.Post

interface PostRemoteDataSource {

    suspend fun getPosts(): List<Post>
}
