package com.dariochamorro.gorillabook.domain.repositories

import com.dariochamorro.gorillabook.domain.common.AsyncResult
import com.dariochamorro.gorillabook.domain.models.Post
import kotlinx.coroutines.flow.Flow

interface PostRepository {

    fun listPosts(): Flow<List<Post>>
    suspend fun loadPost(): AsyncResult<Unit>
    suspend fun insertPost(post: Post): AsyncResult<Unit>
}
