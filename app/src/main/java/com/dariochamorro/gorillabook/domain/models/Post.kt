package com.dariochamorro.gorillabook.domain.models

import java.util.Date

data class Post(
    val id: Long? = null,
    val firstName: String,
    val lastName: String,
    val postBody: String,
    val date: Date,
    val image: String? = null
)
