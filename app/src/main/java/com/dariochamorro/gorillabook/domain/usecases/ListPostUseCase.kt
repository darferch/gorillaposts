package com.dariochamorro.gorillabook.domain.usecases

import com.dariochamorro.gorillabook.domain.models.Post
import com.dariochamorro.gorillabook.domain.repositories.PostRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flowOn
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class ListPostUseCase @Inject constructor(
    private val repository: PostRepository
) {

    operator fun invoke(): Flow<List<Post>> = repository.listPosts()
        .flowOn(Dispatchers.IO)
}
