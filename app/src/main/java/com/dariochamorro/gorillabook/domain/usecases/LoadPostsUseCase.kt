package com.dariochamorro.gorillabook.domain.usecases

import com.dariochamorro.gorillabook.domain.common.AsyncResult
import com.dariochamorro.gorillabook.domain.repositories.PostRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class LoadPostsUseCase @Inject constructor(
    private val repository: PostRepository
) {

    suspend operator fun invoke(): AsyncResult<Unit> = withContext(Dispatchers.Main) {
        repository.loadPost()
    }
}
