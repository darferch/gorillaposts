package com.dariochamorro.gorillabook.domain.usecases

import com.dariochamorro.gorillabook.domain.common.AsyncResult
import com.dariochamorro.gorillabook.domain.models.Post
import com.dariochamorro.gorillabook.domain.repositories.PostRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class AddPostUseCase @Inject constructor(
    private val repository: PostRepository
) {

    suspend operator fun invoke(post: Post): AsyncResult<Unit> = withContext(Dispatchers.Main) {
        repository.insertPost(post)
    }
}
