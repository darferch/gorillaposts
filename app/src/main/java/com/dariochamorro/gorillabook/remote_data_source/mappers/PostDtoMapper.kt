package com.dariochamorro.gorillabook.remote_data_source.mappers

import com.dariochamorro.gorillabook.domain.common.ModelMapper
import com.dariochamorro.gorillabook.domain.models.Post
import com.dariochamorro.gorillabook.remote_data_source.dto.PostDto
import java.util.Date
import javax.inject.Inject

class PostDtoMapper @Inject constructor() : ModelMapper<Post, PostDto>() {
    override fun toModel(data: PostDto): Post = Post(
        id = data.id,
        firstName = data.first_name,
        lastName = data.last_name,
        postBody = data.post_body,
        date = Date(data.unix_timestamp),
        image = urlToSecure(data.image)
    )

    private fun urlToSecure(url: String?): String? {
        return if (url != null) {
            if (url.startsWith("http:")) {
                "https:${url.substring(6, url.length)}"
            } else {
                url
            }
        } else {
            null
        }
    }
}
