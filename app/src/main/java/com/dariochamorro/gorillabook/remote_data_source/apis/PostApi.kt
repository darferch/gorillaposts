package com.dariochamorro.gorillabook.remote_data_source.apis

import com.dariochamorro.gorillabook.remote_data_source.dto.PostDto
import retrofit2.http.GET

interface PostApi {

    @GET("feed")
    suspend fun getPosts(): List<PostDto>
}
