package com.dariochamorro.gorillabook.remote_data_source

import com.dariochamorro.gorillabook.data.datasources.PostRemoteDataSource
import com.dariochamorro.gorillabook.domain.models.Post
import com.dariochamorro.gorillabook.remote_data_source.apis.PostApi
import com.dariochamorro.gorillabook.remote_data_source.mappers.PostDtoMapper
import javax.inject.Inject

class PostRemoteDataSourceImpl @Inject constructor(
    private val api: PostApi,
    private val postDtoMapper: PostDtoMapper
) : PostRemoteDataSource {

    override suspend fun getPosts(): List<Post> {
        val result = api.getPosts()
        return postDtoMapper.toListModel(result)
    }
}
