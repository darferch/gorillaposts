package com.dariochamorro.gorillabook.remote_data_source.dto

data class PostDto(
    val id: Long,
    val first_name: String,
    val last_name: String,
    val post_body: String,
    val unix_timestamp: Long,
    val image: String? = null
)
