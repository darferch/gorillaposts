package com.dariochamorro.gorillabook.presentation.common.errors

import com.dariochamorro.gorillabook.domain.common.Errors

interface ErrorMessage {
    fun getMessage(error: Errors?): String
}
