package com.dariochamorro.gorillabook.presentation.posts

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupWithNavController
import com.dariochamorro.gorillabook.databinding.FragmentPostsBinding
import com.dariochamorro.gorillabook.domain.common.AsyncResultStatus
import com.dariochamorro.gorillabook.domain.common.Errors
import com.dariochamorro.gorillabook.presentation.common.errors.ErrorMessage
import com.dariochamorro.gorillabook.presentation.common.formatters.toLongDate
import dagger.hilt.android.AndroidEntryPoint
import java.util.Date
import javax.inject.Inject

@AndroidEntryPoint
class PostsFragment : Fragment() {

    @Inject
    lateinit var errorMessage: ErrorMessage
    private val viewModel: PostsViewModel by viewModels()
    private val adapter: PostAdapter = PostAdapter()

    private var _binding: FragmentPostsBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentPostsBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupHeader()
        setupToolbar()
        setupRecycler()
        listenAddButton()
        listenPosts()
        listenLoadStatus()
        loadPosts()
    }

    private fun setupHeader() {
        binding.date.text = Date().toLongDate()
    }

    private fun setupToolbar() {
        val navController = findNavController()
        val appBarConfiguration = AppBarConfiguration(navController.graph)
        binding.toolbar
            .setupWithNavController(navController, appBarConfiguration)
    }

    private fun setupRecycler() {
        binding.recycler.adapter = adapter
    }

    private fun listenAddButton() {
        binding.buttonAddPost.setOnClickListener {
            goToAddPost()
        }
    }

    private fun listenPosts() {
        viewModel.posts.observe(viewLifecycleOwner) {
            adapter.data = it
        }
    }

    private fun listenLoadStatus() {
        viewModel.loadPostsStatus.observe(viewLifecycleOwner) {
            when (it.status) {
                AsyncResultStatus.LOADING -> showLoader()
                AsyncResultStatus.SUCCESS -> {
                    hideLoader()
                }
                AsyncResultStatus.ERROR -> {
                    hideLoader()
                    showError(it.error)
                }
            }
        }
    }

    private fun loadPosts() {
        hideError()
        viewModel.loadPosts()
    }

    private fun showLoader() {
        binding.progressBar.visibility = View.VISIBLE
    }

    private fun hideLoader() {
        binding.progressBar.visibility = View.GONE
    }

    private fun showError(error: Errors?) {
        val message = errorMessage.getMessage(error)
        binding.recycler.visibility = View.GONE
        binding.error.root.visibility = View.VISIBLE
        binding.error.errorMessage.text = message
        binding.error.errorRetry.setOnClickListener {
            loadPosts()
        }
    }

    private fun hideError() {
        binding.recycler.visibility = View.VISIBLE
        binding.error.root.visibility = View.GONE
    }

    private fun goToAddPost() {
        val action = PostsFragmentDirections.actionPostToAddPost()
        findNavController().navigate(action)
    }
}
