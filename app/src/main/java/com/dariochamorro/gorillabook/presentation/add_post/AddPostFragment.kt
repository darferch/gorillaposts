package com.dariochamorro.gorillabook.presentation.add_post

import android.os.Bundle
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.widget.Toolbar
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupWithNavController
import com.dariochamorro.gorillabook.R
import com.dariochamorro.gorillabook.databinding.FragmentAddPostBinding
import com.dariochamorro.gorillabook.domain.common.AsyncResultStatus
import com.dariochamorro.gorillabook.domain.models.Post
import dagger.hilt.android.AndroidEntryPoint
import java.util.Date

@AndroidEntryPoint
class AddPostFragment : Fragment(), Toolbar.OnMenuItemClickListener {

    private val viewModel: AddPostViewModel by viewModels()

    private var _binding: FragmentAddPostBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentAddPostBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupToolbar()
        listenAddPostStatus()
    }

    private fun setupToolbar() {
        val navController = findNavController()
        val appBarConfiguration = AppBarConfiguration(navController.graph)
        binding.toolbar
            .setupWithNavController(navController, appBarConfiguration)

        binding.toolbar.inflateMenu(R.menu.menu_add)
        binding.toolbar.setOnMenuItemClickListener(this)
    }

    private fun listenAddPostStatus() {
        viewModel.addPostStatus.observe(viewLifecycleOwner) {
            when (it.status) {
                AsyncResultStatus.LOADING -> showLoader()
                AsyncResultStatus.SUCCESS -> {
                    hideLoader()
                    goToBack()
                }
                AsyncResultStatus.ERROR -> {
                }
            }
        }
    }

    private fun showLoader() {
        binding.progressBar.visibility = View.VISIBLE
    }

    private fun hideLoader() {
        binding.progressBar.visibility = View.GONE
    }

    private fun addPost() {
        val body = binding.inputBody.editText?.text.toString()
        if (body == "") {
            Toast.makeText(requireContext(), R.string.body_not_empty, Toast.LENGTH_SHORT).show()
        }

        val post = Post(
            firstName = "Dario",
            lastName = "Chamorro",
            date = Date(),
            postBody = body
        )
        viewModel.addPost(post)
    }

    override fun onMenuItemClick(item: MenuItem?): Boolean {
        addPost()
        return false
    }

    private fun goToBack() {
        findNavController().popBackStack()
    }
}
