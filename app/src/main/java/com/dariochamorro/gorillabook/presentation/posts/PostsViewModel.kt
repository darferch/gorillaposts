package com.dariochamorro.gorillabook.presentation.posts

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.asLiveData
import androidx.lifecycle.viewModelScope
import com.dariochamorro.gorillabook.domain.common.AsyncResult
import com.dariochamorro.gorillabook.domain.models.Post
import com.dariochamorro.gorillabook.domain.usecases.ListPostUseCase
import com.dariochamorro.gorillabook.domain.usecases.LoadPostsUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class PostsViewModel @Inject constructor(
    listPostUseCase: ListPostUseCase,
    private val loadPostsUseCase: LoadPostsUseCase
) : ViewModel() {

    val posts: LiveData<List<Post>> = listPostUseCase().asLiveData()

    private val loadPostsStatusControl: MutableLiveData<AsyncResult<Unit>> = MutableLiveData()
    val loadPostsStatus: LiveData<AsyncResult<Unit>> = loadPostsStatusControl

    fun loadPosts() = viewModelScope.launch(Dispatchers.Main) {
        loadPostsStatusControl.value = AsyncResult.loading()
        loadPostsStatusControl.value = loadPostsUseCase()
    }
}
