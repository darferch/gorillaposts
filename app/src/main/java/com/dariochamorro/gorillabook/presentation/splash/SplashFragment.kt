package com.dariochamorro.gorillabook.presentation.splash

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.dariochamorro.gorillabook.R
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

class SplashFragment : Fragment(R.layout.fragment_splash) {

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        loadSplash()
    }

    private fun loadSplash() = lifecycleScope.launch {
        delay(1500)
        val action = SplashFragmentDirections.actionSplashToPost()
        findNavController().navigate(action)
    }
}
