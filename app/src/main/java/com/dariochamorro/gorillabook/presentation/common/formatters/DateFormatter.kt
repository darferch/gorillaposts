package com.dariochamorro.gorillabook.presentation.common.formatters

import java.text.SimpleDateFormat
import java.util.Date
import java.util.Locale

fun Date.toShortDate(): String {
    val formatter = SimpleDateFormat("MM/dd/yyyy", Locale.getDefault())
    return formatter.format(this)
}

fun Date.toLongDate(): String {
    val formatter = SimpleDateFormat("E, M d", Locale.getDefault())
    return formatter.format(this)
}
