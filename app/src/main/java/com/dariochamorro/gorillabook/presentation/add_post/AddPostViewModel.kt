package com.dariochamorro.gorillabook.presentation.add_post

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.dariochamorro.gorillabook.domain.common.AsyncResult
import com.dariochamorro.gorillabook.domain.models.Post
import com.dariochamorro.gorillabook.domain.usecases.AddPostUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class AddPostViewModel @Inject constructor(
    private val addPostUseCase: AddPostUseCase
) : ViewModel() {

    private val addPostStatusControl: MutableLiveData<AsyncResult<Unit>> = MutableLiveData()
    val addPostStatus: LiveData<AsyncResult<Unit>> = addPostStatusControl

    fun addPost(post: Post) = viewModelScope.launch(Dispatchers.Main) {
        addPostStatusControl.value = AsyncResult.loading()
        addPostStatusControl.value = addPostUseCase(post)
    }
}
