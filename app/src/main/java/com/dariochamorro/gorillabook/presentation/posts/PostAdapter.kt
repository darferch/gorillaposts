package com.dariochamorro.gorillabook.presentation.posts

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.dariochamorro.gorillabook.R
import com.dariochamorro.gorillabook.databinding.ItemPostBinding
import com.dariochamorro.gorillabook.domain.models.Post
import com.dariochamorro.gorillabook.presentation.common.formatters.toShortDate
import com.dariochamorro.gorillabook.presentation.common.images.loadUrl

class PostAdapter : RecyclerView.Adapter<PostAdapter.PostViewHolder>() {

    var data: List<Post> = emptyList()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PostViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_post, parent, false)
        return PostViewHolder(view)
    }

    override fun onBindViewHolder(holder: PostViewHolder, position: Int) {
        holder.bind(data[position])
    }

    override fun getItemCount(): Int = data.size

    class PostViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        private val binding = ItemPostBinding.bind(view)

        fun bind(post: Post) {
            binding.postBody.text = post.postBody
            binding.postName.text = itemView.resources.getString(
                R.string.user_name_format,
                post.firstName,
                post.lastName
            )
            binding.postDate.text = post.date.toShortDate()
            binding.postImage.loadUrl(post.image)
        }
    }
}
